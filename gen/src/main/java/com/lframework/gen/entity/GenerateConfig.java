package com.lframework.gen.entity;

import com.lframework.starter.mybatis.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 代码生成信息
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GenerateConfig extends BaseEntity {

}
